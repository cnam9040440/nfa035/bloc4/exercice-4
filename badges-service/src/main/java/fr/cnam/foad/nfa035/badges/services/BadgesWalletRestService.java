package fr.cnam.foad.nfa035.badges.services;

import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestPart;

import java.util.Set;

public interface BadgesWalletRestService {

    /**
     * suppression d'un badge
     * @param digitalBadge
     * @return
     */
    ResponseEntity deleteBadge(DigitalBadge digitalBadge);

    /**
     * Lecture du Wallet => R
     *
     * @return ResponseEntity la réponse REST toujours
     */
    @Operation(summary = "Récupère le métadonnées du Wallet",
            description = "Récupère le métadonnées du portefeuille, c'est à dire l'index des badges qui s'y trouve"
    )
    @Tag(name = "getMetadata")
    @GetMapping("/metas")
    ResponseEntity<Set<DigitalBadge>> getMetadata();


    /**
     * ajout d'un badge
     * @param digitalBadge
     * @return
     */
    ResponseEntity putBadge(DigitalBadge digitalBadge);

    /**
     * lecture d'un badge
     * @param badge
     * @return
     */
    ResponseEntity<DigitalBadge> readBadge(DigitalBadge badge);
}
