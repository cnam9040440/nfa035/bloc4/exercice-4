package fr.cnam.foad.nfa035.badges.services.impl;

import fr.cnam.foad.nfa035.badges.services.BadgesWalletRestService;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Set;
import java.util.SortedMap;

@RestController
public class BadgesWalletRestServiceImpl implements BadgesWalletRestService  {

    @Qualifier("jsonBadge")
    @Autowired
    private JSONBadgeWalletDAOImpl jsonBadgeDao;


    /**
     * {@inheritDoc}
     * @param digitalBadge
     * @return
     */
    @Override
    public ResponseEntity deleteBadge(fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge digitalBadge) {
        return null;
    }

    /**
     * Lecture du Wallet => R
     *
     * @return ResponseEntity la réponse REST toujours
     */
    @Override
    public ResponseEntity<Set<DigitalBadge>> getMetadata() {

        try {
            return ResponseEntity
                    .ok()
                    .body(jsonBadgeDao.getWalletMetadata());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

    /**
     * {@inheritDoc}
     * @param digitalBadge
     * @return
     */
    @Override
    public ResponseEntity putBadge(fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge digitalBadge) {
        return null;
    }

    /**
     * {@inheritDoc}
     * @param badge
     * @return
     */
    @Override
    public ResponseEntity<DigitalBadge> readBadge(DigitalBadge badge) {
        return null;
    }

}
